<?php

$thedata = json_decode(file_get_contents('php://input'), true);
$a = new clientcall($thedata['baseline'],$thedata['total'],$thedata['startdate'], $thedata['enddate']);
print("<br/><br/><br/><br/><pre>".print_r($a->results(),true)."</pre>");

//Parent Class Start
abstract class DataProcessor {
    
    protected $alldays = array();
    protected $allworkingd = array();
    protected $numallwokgd = '';
    protected $prcrandarry = array();
    
    public function __construct($baseline, $total, $start, $end) {
        $this->alldateprocessor($start, $end);
        $this->customdivide($baseline, $total);
    }
    
//    Divide Total amount into unequal n amounts
    public function customdivide($baseline, $total) {
            $i = $this->numallwokgd;
            $num = array();
            $n = 1;
            $sum = 0;
            $min = round($baseline / $i, 2);
            $target = ($total - ($min * $i)) * 100;
            $low = 0;
            $high = (float)$target * 0.4;

            while($n < $i)
            {
                do
                {
                    $t = mt_rand($low, $high);
                }while($sum + $t >= $target);
                $num[] = number_format(($t * 0.01) + $min, 2);
                $sum += $t;
                $n++;
            }
            $num[] = number_format((($target - $sum) * 0.01) + $min, 2);
            shuffle($num);
            $this->prcrandarry = $num;
//            return $num;
    }
    
//   All One functioning Unit for Date
    public function alldateprocessor($start, $end) {
        // Loop between timestamps, 1 day at a time 
        $begin = new DateTime( $start );
        $end = new DateTime( $end );
        $end = $end->modify( '+1 day' ); 

        $interval = new DateInterval('P1D');
        $daterange = new DatePeriod($begin, $interval ,$end);

        foreach($daterange as $date){
            //    echo $date->format("Y-m-d") . "<br>";
        $dates[] = $date->format("Y-m-d");
        if(date("w", strtotime($date->format("Y-m-d"))) == 0 || date("w", strtotime($date->format("Y-m-d"))) == 6) {
            $datess[] = $date->format("Y-m-d");
            }
    
        }
        $result = array_diff($dates, $datess);
        $result = array_flip($result);
        $datess = array_flip($datess);
        $dates = array_flip($dates);
        $datess = array_fill_keys(array_keys($datess), 0);
        $resultallweekendzero = array_merge($dates, $datess);
//        All Days but with week end zero
        $this->alldays = $resultallweekendzero;
//        All working days array
        $this->allworkingd = $result;
//        Number of working Days
        $this->numallwokgd = count($result);  

    }
    
//    All Grinder
    public function allgrinder() {
        $j = 0;
        foreach($this->allworkingd as $key => $value){
			$resultnowdistri[$key] = number_format(doubleval(str_replace(",","",$this->prcrandarry[$j])),2);
            $j++;
	    }
        $resultfinal = array_merge($this->alldays, $resultnowdistri);
        return $resultfinal;
        
    }
    
    abstract protected function results();
    
    public function __destruct() {
    }
}

//Calling parent class for data processing
class clientcall extends DataProcessor {
    public function __construct($baseline, $total, $start, $end) {
        parent::__construct( $baseline, $total, $start, $end);
    }
    public function results() {
        return $this->allgrinder();
    }
    public function __destruct() {
    }
}

?> 